<?php

namespace Tests\Unit;

# use PHPUnit\Framework\TestCase;
use App\DefaultTeamCalculator;
use App\DraftStyleTeamsBuilder;
use Tests\TestCase;

use App\User;

class PlayersIntegrityTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGoaliePlayersExist ()
    {
/*
		Check there are players that have can_play_goalie set as 1
*/
		$result = User::players()->goalies()->count();
		$this->assertTrue($result > 1);
    }

    public function testAtLeastOneGoaliePlayerPerTeam ()
    {
/*
	    calculate how many teams can be made so that there is an even number of teams and they each have between 18-22 players.
	    Then check that there are at least as many players who can play goalie as there are teams
*/
        $min_player_count = 18;
        $max_player_count = 22;

        $team_size_calculator = new DefaultTeamCalculator();

        $team_count = $team_size_calculator
            ->getTeamsCount($goalies_count=25, $player_count=500, $min_player_count, $max_player_count);
        $this->assertTrue($team_count % 2 == 0);

        // fill the groups.
        //
        $groups = [];
        foreach(range(0, $team_count-1) as $i)
        {
            $groups[$i] = [];
        }

        // fill to test distribution.
        //
        foreach(range(0, 499) as $index => $item)
        {
            $groups[$index % $team_count][] = $item;
        }

        foreach($groups as $group)
        {
            $this->assertTrue(count($group) >= $min_player_count && count($group) <= $max_player_count);
        }

        $this->assertTrue(25 >= $team_count);
    }

    public function testAtLeastOneGoaliePlayerPerTeamUsingDb()
    {
        $min_player_count = 18;
        $max_player_count = 22;

        $players = User::players()->get();
        $goalies = User::players()->goalies()->get();
        $not_goalies = User::players()->notGoalies()->get();

        // players should equal goalies + no goalies
        //
        $this->assertTrue($players->count() == $goalies->count() + $not_goalies->count());

        // Calculate the # of teams.
        //
        $team_size_calculator = new DefaultTeamCalculator();
        $team_count = $team_size_calculator
            ->getTeamsCount($goalies->count(), $players->count(), $min_player_count, $max_player_count);

        // mist be event # of teams.
        //
        $this->assertTrue($team_count % 2 == 0);

        // fill the teams.
        //
        $team_builder = new DraftStyleTeamsBuilder();
        $teams = $team_builder->build($team_count, $goalies, $not_goalies);

        $total_count = 0;
        foreach($teams as $team)
        {
            $total_count += count($team->getPlayers());
            $this->assertTrue(count($team->getPlayers()) >= $min_player_count && count($team->getPlayers()) <= $max_player_count);
        }

        // all players were accounted for?
        //
        $this->assertTrue($total_count == $players->count());

        // do we have enough goalies for the teams.
        //
        $this->assertTrue($goalies->count() >= $team_count);
    }
}
