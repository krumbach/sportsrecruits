<?php


namespace App;


interface ITeamsBuilder
{
    public function build($team_count, $specialPlayers, $players);
}
