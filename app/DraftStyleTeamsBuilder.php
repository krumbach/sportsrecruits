<?php


namespace App;


use Mockery\Exception;
use Faker\Factory as Faker;

/**
 * Class DraftStyleTeamsBuilder
 * Basic team creator using alternating draft order.
 */
class DraftStyleTeamsBuilder implements ITeamsBuilder
{
    public function build($team_count, $specialPlayers, $players)
    {
        if(count($specialPlayers) < $team_count)
        {
            throw new Exception("not enough special players for the number of teams");
        }

        // Create the teams.
        //
        $teams = $this->createTeams($team_count);

        // Distribute the special players, one per team. the remaining will
        // go into the pool of players to be distributed.
        //
        $specialPlayersCollection = $this->sortByRanking(collect($specialPlayers));
        $toDistribute = $specialPlayersCollection->splice(0, $team_count);
        $this->distributePlayers($teams, $toDistribute);

        // take the left-over special players and add it remaining player list.
        //
        $playersCollection = collect($players);
        $playersCollection = $playersCollection->concat($specialPlayersCollection);
        $playersCollection = $this->sortByRanking($playersCollection);

        // distribute the remaining players.
        //
        $toDistribute = $playersCollection->splice(0, $team_count);

        while($toDistribute->count() > 0)
        {
            $this->distributePlayers($teams, $toDistribute);

            $toDistribute = $playersCollection->splice(0, $team_count);

            $teams = $teams->reverse();
        }

        return $teams;
    }

    private function createTeams($count)
    {
        $teams = [];
        $faker = Faker::create();
        foreach (range(0, $count-1) as $index)
        {
            $name = "{$faker->city} {$faker->colorName}s";
            $teams[] = new Team("{$name}");
        }

        return collect($teams);
    }

    private function sortByRanking($collection)
    {
        return $collection->sortByDesc(function ($item, $key) {
            return $item['ranking'];
        });
    }

    private function distributePlayers($teams, $players)
    {
        $index = 0;
        foreach ($teams as $team)
        {
            if(isset($players[$index]))
            {
                $team->addPlayer($players[$index]);
            }

            $index++;
        }
    }
}
