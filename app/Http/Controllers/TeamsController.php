<?php

namespace App\Http\Controllers;

use App\ITeamCalculator;
use App\ITeamsBuilder;
use App\User;

class TeamsController extends Controller
{
    private $team_calculator;
    private $team_builder;

    public function __construct(ITeamCalculator $calculator, ITeamsBuilder $team_builder)
    {
        $this->team_calculator = $calculator;
        $this->team_builder = $team_builder;
    }

    public function index()
    {
        $min_player_count = 18;
        $max_player_count = 22;

        // get the inputs for the calculations.
        //
        $players = User::players()
            ->notGoalies()
            ->byRank()
            ->get();

        $goalies = User::players()
            ->goalies()
            ->byRank()
            ->get();

        $total_players = $players->count() + $goalies->count();

        // Given the constraints calculate the number of teams.
        //
        $teams_count = $this->team_calculator
            ->getTeamsCount(
                $goalies->count(),
                $total_players,
                $min_player_count,
                $max_player_count);

        // Distribute the players to the teams based on their rankings.
        //
        $teams = $this->team_builder
            ->build($teams_count, $goalies, $players);

        return view('teams.index', ['teams' => $teams]);
    }
}
