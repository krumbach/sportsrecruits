<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * Scope a query to only include player users.
     */
    public function scopePlayers($query)
    {
        return $query->where('user_type', 'player');
    }

    /**
     * Scope a query to only include goalie users.
     */
    public function scopeGoalies($query)
    {
        return $query->where('can_play_goalie', true);
    }

    /**
     * Scope a query to only include not goalie users.
     */
    public function scopeNotGoalies($query)
    {
        return $query->where('can_play_goalie', false);
    }

    /**
     * Scope a query to order players by rank descending.
     */
    public function scopeByRank($query)
    {
        return $query->orderBy('ranking', 'desc');
    }

    /**
     * User full name attribute.
     */
    public function getFullNameAttribute()
    {
        return "{$this->last_name}, {$this->first_name}";
    }
}
