<?php


namespace App;


interface ITeamCalculator
{
    public function getTeamsCount($specialPlayersCount, $playerCount, $minPlayers, $maxPlayer);
}
