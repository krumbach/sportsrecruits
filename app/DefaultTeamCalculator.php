<?php


namespace App;


class DefaultTeamCalculator implements ITeamCalculator
{
    public function getTeamsCount($special_players_count, $player_count, $min_team_players, $max_team_players)
    {
        $min_num_groups = (int)($player_count / $max_team_players);
        $max_num_groups = (int)($player_count / $min_team_players);

        $count = $min_num_groups;
        while($count <= $max_num_groups)
        {
            if($count % 2 == 0 && $count <= $special_players_count)
            {
                $group_size = (int)($player_count / $count);

                $items_to_distribute = $player_count - $count * $group_size;
                $carry_over = $group_size + $items_to_distribute / $count;

                if($group_size <= $max_team_players && $carry_over <= $max_team_players)
                {
                    break;
                }
            }

            $count += 1;
        }

        return $count;
    }
}
