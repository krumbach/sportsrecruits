<?php


namespace App;


class Team
{
    private $name;
    private $players;

    public function __construct($name)
    {
        $this->name = $name;
        $this->players = [];
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPlayers($sortedByLastName = true)
    {
        if($sortedByLastName)
        {
            $collection = collect($this->players);

            $sorted = $collection->sortBy(function ($player, $key) {
                return $player['last_name'];
            });

            return $sorted;
        }

        return $this->players;
    }

    public function addPlayer(User $player)
    {
        $this->players[] = $player;
    }

    public function ranking()
    {
        return collect($this->players)
            ->sum('ranking');
    }
}
