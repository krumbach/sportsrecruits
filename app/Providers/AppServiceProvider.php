<?php

namespace App\Providers;

use App\DefaultTeamCalculator;
use App\DraftStyleTeamsBuilder;
use App\ITeamCalculator;
use App\ITeamsBuilder;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Could have multiple implementations and have it set in a config value for which
        // one to use. The other option is to use a factory and inject that into the controller
        // and local logic could be used to request different implementations.
        //
        $this->app->singleton(ITeamCalculator::class, function ($app) {
            return new DefaultTeamCalculator();
        });

        $this->app->singleton(ITeamsBuilder::class, function ($app) {
            return new DraftStyleTeamsBuilder();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
