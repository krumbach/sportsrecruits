@extends('layouts.master')

@section('layout')
    <div class="min-h-screen flex flex-col">
        <main class="flex-grow">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 mb-7">
                @yield('content')
            </div>
        </main>

        <footer>
            @yield('footer')
        </footer>
    </div>
@endsection
