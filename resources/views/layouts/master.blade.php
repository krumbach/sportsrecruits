<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="bg-gray-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/1.5.2/tailwind.css" rel="stylesheet">

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    @yield('head')
</head>
<body class="antialiased">
    <div id="app">
        @yield('layout')
    </div>

    @stack('scriptsBefore')
    <script src="{{ asset('js/app.js') }}"></script>
    @stack('scriptsAfter')
</body>
</html>
