<div class="block sm:w-1/2 md:w-1/3 mb-3">
    <div class="bg-white overflow-hidden shadow rounded-lg mx-2 sm:ml-3">
        <div class="border-b border-gray-200 px-4 py-5 sm:px-6">
            <h2 class="text-xl font-semibold">
                {{ $team->getName() }} <span class="text-gray-600 font-medium">rank ({{ $team->ranking() }})</span></h2>
        </div>
        <div class="px-4 py-5 sm:p-6">
            <h3 class="text-xl mb-3">Players ({{ $team->getPlayers()->count() }})</h3>
            <ul class="">
                @foreach($team->getPlayers() as $player)
                    <li>
                        {{ $player->fullName }}
                        @if($player->can_play_goalie)
                            <span>*</span>
                        @endif
                        ({{ $player->ranking }})
                    </li>
                @endforeach
            </ul>
            <div class="italic mt-3 text-gray-600">* goalie position</div>
        </div>
    </div>
</div>
