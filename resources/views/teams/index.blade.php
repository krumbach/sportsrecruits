@extends('layouts.app')

@section('content')
    <h1 class="text-4xl font-semibold m-4">Teams ({{ $teams->count() }})</h1>

    <div class="block sm:flex sm:flex-wrap">
        @foreach($teams as $team)
            @include('teams._team')
        @endforeach
    </div>
@stop
